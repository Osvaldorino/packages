# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=wpa_supplicant
pkgver=2.9
pkgrel=1
pkgdesc="Utility providing key negotiation for WPA wireless networks"
url="https://w1.fi/wpa_supplicant/"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause AND Public-Domain AND (GPL-2.0+ OR BSD-3-Clause)"
depends=""
makedepends="linux-headers openssl-dev dbus-dev libnl3-dev qt5-qtbase-dev"
subpackages="$pkgname-dbus::noarch $pkgname-doc $pkgname-openrc wpa_gui"
source="https://w1.fi/releases/$pkgname-$pkgver.tar.gz
	wpa_supplicant.initd
	wpa_supplicant.confd
	wpa_supplicant.conf
	dbus.patch
	eloop.patch
	CVE-2019-16275.patch

	config
	wpa_cli.sh"

# secfixes:
#   2.6-r7:
#     - CVE-2017-13077
#     - CVE-2017-13078
#     - CVE-2017-13079
#     - CVE-2017-13080
#     - CVE-2017-13081
#     - CVE-2017-13082
#     - CVE-2017-13086
#     - CVE-2017-13087
#     - CVE-2017-13088
#   2.9-r1:
#     - CVE-2019-16275

prepare() {
	default_prepare

	# Copy our configuration file to the build directory
	cp "$srcdir"/config "$builddir"/wpa_supplicant/.config
}

build() {
	cd "$builddir"/wpa_supplicant
	make LIBDIR=/lib BINDIR=/sbin all eapol_test

	# wpa_gui
	qmake -o wpa_gui-qt4/Makefile wpa_gui-qt4/wpa_gui.pro
	make -C wpa_gui-qt4
}

package() {
	cd "$builddir"/wpa_supplicant
	make DESTDIR="$pkgdir" LIBDIR=/lib BINDIR=/sbin install
	install -Dm644 wpa_supplicant.conf \
		"$pkgdir"/usr/share/doc/wpa_supplicant/examples/wpa_supplicant.conf
	install -Dm755 "$srcdir"/wpa_cli.sh \
		"$pkgdir"/etc/wpa_supplicant/wpa_cli.sh

	local man=
	for man in doc/docbook/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done
	install -Dm755 eapol_test "$pkgdir"/sbin/eapol_test

	install -Dm755 "$srcdir"/wpa_supplicant.initd \
		"$pkgdir"/etc/init.d/wpa_supplicant
	install -Dm644 "$srcdir"/wpa_supplicant.confd \
		"$pkgdir"/etc/conf.d/wpa_supplicant
	install -Dm640 "$srcdir"/wpa_supplicant.conf \
		"$pkgdir"/etc/wpa_supplicant/wpa_supplicant.conf

	# work around netifrc braindamage
	mkdir -p "$pkgdir"/bin
	ln -s ../sbin/wpa_cli "$pkgdir"/bin/wpa_cli
}

dbus() {
	pkgdesc="$pkgdesc (dbus services)"
	depends="$pkgname dbus"
	install_if="$pkgname=$pkgver-r$pkgrel dbus"

	cd "$builddir/wpa_supplicant/dbus"
	install -d "$subpkgdir"/etc/dbus-1/system.d
	install -m644 dbus-wpa_supplicant.conf \
		"$subpkgdir"/etc/dbus-1/system.d/wpa_supplicant.conf
	install -d "$subpkgdir"/usr/share/dbus-1/system-services
	install fi.w1.wpa_supplicant1.service \
		"$subpkgdir"/usr/share/dbus-1/system-services
}

wpa_gui() {
	pkgdesc="Graphical User Interface for wpa_supplicant"
	depends="$pkgname"

	cd "$builddir"/wpa_supplicant
	install -Dm755 wpa_gui-qt4/wpa_gui "$subpkgdir"/usr/bin/wpa_gui
}

sha512sums="37a33f22cab9d27084fbef29856eaea0f692ff339c5b38bd32402dccf293cb849afd4a870cd3b5ca78179f0102f4011ce2f3444a53dc41dc75a5863b0a2226c8  wpa_supplicant-2.9.tar.gz
11eed22f6e793f40c788d586c715deecae03c421d11761b7b4a376660bce812c54cc6f353c7d4d5da9c455aeffd778baefb9e76d380027a729574a756e54ddcc  wpa_supplicant.initd
29103161ec2b9631fca9e8d9a97fafd60ffac3fe78cf613b834395ddcaf8be1e253c22e060d7d9f9b974b2d7ce794caa932a2125e29f6494b75bce475f7b30e1  wpa_supplicant.confd
f8b224b6c5a8adf378d8224beb49f2a99817d303f7e6a724943ecb3313ae85ce0fdd8291a20c95563470681ebf5d991ffa31094b9171e470e9690b38bba25738  wpa_supplicant.conf
dac56bc505a51167042ebe548f0e81a20a5578f753af9bb7ec3335a542d799c6e8739681ef7c8f7747a9bc954f8aa6f1a147250eacba17fd7fff80c4e53638ed  dbus.patch
2be055dd1f7da5a3d8e79c2f2c0220ddd31df309452da18f290144d2112d6dbde0fc633bb2ad02c386a39d7785323acaf5f70e5969995a1e8303a094eb5fe232  eloop.patch
63710cfb0992f2c346a9807d8c97cbeaed032fa376a0e93a2e56f7742ce515e9c4dfadbdb1af03ba272281f639aab832f0178f67634c222a5d99e1d462aa9e38  CVE-2019-16275.patch
221660fa0350442a7d8371686b2118861052a4613fb352b7f80079e3750b82f4e48efc378b9d617455007d1106552b695fdca506a3c338283986641f3848b202  config
45d3e70c47d0f7d6dc6730853af8cbcb40ed0713ee7b1069698f5a635939f273f66e72d4221e064c3c71a92154cf07841c8c0d4fc14d796dbb6fe0d92776ee2b  wpa_cli.sh"
