# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ode
pkgver=0.16.1
pkgrel=0
pkgdesc="High performance library for simulating rigid body dynamics"
url="https://ode.org/"
arch="all"
options="!check"  # Fails 48/52 tests
license="LGPL-2.1+ OR BSD-3-Clause"
depends=""
makedepends="autoconf automake libtool"
subpackages="$pkgname-dev"
source="https://bitbucket.org/odedevs/ode/downloads/ode-$pkgver.tar.gz
	fix-test-link.patch
	stdint.patch
	"

build() {
	./bootstrap
	CXXFLAGS="$CXXFLAGS -fpermissive" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared \
		--disable-static \
		--disable-double-precision \
		--enable-libccd \
		--enable-ou
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="65aa7b6baf98bab9aa721a119246371e31e070d97770a8cdaea882de3cd5e2dfde7ca1b4258186b397853cc0dc3b2f7b6b7aef3ac86b48a72a3011d5ae536672  ode-0.16.1.tar.gz
8630d5d059fd0f623db6af4000666868358002a42ba84817117b1fb5e01c776bb23cbf1c8c43181d7bf40a0d71b640f9d2f9785461d8a77877dcbdadd775792e  fix-test-link.patch
0d618cec0afdab86279687275bf476594897d1dfd7b5619fe6bc05343f22f6292c9598a73ff6f2967d5f39e071abcf282fc673c0ffe33f7efd9bbe56ca674dae  stdint.patch"
