# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sddm
pkgver=0.18.1
pkgrel=3
pkgdesc="Simple Desktop Display Manager"
url="https://github.com/sddm/sddm/"
pkgusers="sddm"
pkggroups="sddm"
arch="all"
license="GPL-2.0+"
depends="dbus-x11 elogind"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxcb-dev upower-dev
	elogind-dev linux-pam-dev qt5-qtdeclarative-dev qt5-qttools-dev
	utmps-dev"
subpackages="$pkgname-lang $pkgname-openrc"
install="sddm.post-install"
langdir="/usr/share/sddm/translations"
source="https://github.com/sddm/sddm/releases/download/v$pkgver/sddm-$pkgver.tar.xz
	pam-path-fix.patch
	sddm.initd
	utmpx.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_JOURNALD=OFF \
		-DNO_SYSTEMD=ON \
		-DUSE_ELOGIND=ON \
		-DUID_MIN=500 \
		-DUID_MAX=65000 \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
	install -D -m755 "$srcdir"/sddm.initd "$pkgdir"/etc/init.d/sddm
}

openrc() {
	default_openrc
	depends="sddm xorg-server"
}

sha512sums="ff0637600cda2f4da1f643f047f8ee822bd9651ae4ccbb614b9804175c97360ada7af93e07a7b63832f014ef6e7d1b5380ab2b8959f8024ea520fa5ff17efd60  sddm-0.18.1.tar.xz
f0b4eb7ef0581701157f9decc637629156f36f6711b9a4bae517f94d7a1df614c81bbd891c918f07ac50e2a3d1519c43ccb9eefd80282c95dd79eca0e8d90904  pam-path-fix.patch
d603934552bad47edda458d7a4df2310e98bde74bdb3bf8588f5171b2a5d68814192b8dc8f5599b35402f9a747d519d985d4976e7aa50dabed445f99a112594c  sddm.initd
c42d8b3edbc0ae7e3d5ea7bb0080c5c50e0569f0ea947e1ba17bc794c8c0d67a214e62aad7eba0a51791c44b29a3017692bbe738250c63cb2219891bb1313422  utmpx.patch"
