# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer:
pkgname=mlt
pkgver=6.22.1
pkgrel=0
pkgdesc="MLT multimedia framework"
url="https://www.mltframework.org/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+ AND GPL-3.0+"
depends=""
makedepends="bsd-compat-headers ffmpeg-dev fftw-dev frei0r-plugins-dev
	gtk+2.0-dev libexif-dev libsamplerate-dev libxml2-dev sdl-dev sdl2-dev
	qt5-qtbase-dev qt5-qtsvg-dev sdl_image-dev sox-dev"
subpackages="$pkgname-dev"
source="https://github.com/mltframework/mlt/releases/download/v$pkgver/mlt-$pkgver.tar.gz
	mlt-6.14.0-locale-header.patch
	"

build() {
	_maybe_asm=""

	case $CTARGET_ARCH in
		pmmx)  _maybe_asm="--disable-sse --target-arch=i586" ;;
		ppc)   export LDFLAGS="$LDFLAGS -latomic" ;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--avformat-swscale \
		--enable-motion-est \
		--enable-gpl \
		--enable-gpl3 \
		--disable-rtaudio \
		$_maybe_asm
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c620b68d35c90eab650c70768a4ae631dec83ece6dd3fd8e09f9300d837d8e0f3da1b098786188f9c1216800f848dd5db7c9e5fa03e816fba3fbcf3c63324c74  mlt-6.22.1.tar.gz
d00f01d50d5c78b1da5b43dc2b0bbfc49d5e383b602169ae9554734d29f6d43b9da8f97546141933c06ff0367bb4c9f0d2161bbcb6f016265bb0aa1dcbfcb3b1  mlt-6.14.0-locale-header.patch"
