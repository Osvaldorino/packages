# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmailtransport
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE email transport library"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kcmutils-dev kmime-dev
	kwallet-dev akonadi-dev akonadi-mime-dev ksmtp-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmailtransport-$pkgver.tar.xz
	degoogled.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	# Akonadi tests require D-Bus.
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'akonadi-*'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="409475cf4a34b15f7f2bed4d5e7612c8c72e90cb39cc778ce79f1132180cde311a2d490e2172787c1461ca017348faff74fc10203aa482f51ef81550a15e310e  kmailtransport-20.08.1.tar.xz
0fbde4a8983be652ee10cd1c64391240c80ccaae697c77c43f8c9bc0bfd85cbe0dd37a5fc6860185ed15575c8bfcd198276c8291bbfc83bb1811a57eea5ea6a7  degoogled.patch"
