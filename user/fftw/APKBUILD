# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: 
pkgname=fftw
pkgver=3.3.8
_pkgver=$(printf '%s' "$pkgver" | sed 's/p/-pl/')
pkgrel=0
pkgdesc="Fastest fourier transform in the west"
url="http://www.fftw.org/"
# s390x: hangs on container and KVM
arch="all !s390x"
license="GPL-2.0+"
makedepends="texinfo"
# order of the libs split functions are important because of lib naming
subpackages="$pkgname-dev $pkgname-doc $pkgname-single-libs:single
	$pkgname-long-double-libs:long_double $pkgname-double-libs:double"
source="http://www.fftw.org/$pkgname-$_pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

_precision="single double long-double"

prepare() {
	default_prepare

	local i; for i in $_precision; do
		cp -r "$builddir" $srcdir/$i
	done
}

build() {
	local _openmp=

	local i; for i in $_precision; do
		case "$i" in
			single) _cf="--enable-single";;
			double) _cf="";;
			long-double) _cf="--enable-long-double";;
		esac

		case "$i--$CARCH" in
			single--x86_64 | double--x86_64)
				_cf="$_cf --enable-sse2 --enable-avx";;
			single--arm* | single--aarch64)
				_cf="$_cf --enable-neon";;
		esac

		msg "Building for $i precision ($_cf)"
		cd "$srcdir"/$i
		./configure \
			--build=$CBUILD \
			--host=$CHOST \
			--prefix=/usr \
			--sysconfdir=/etc \
			--mandir=/usr/share/man \
			--infodir=/usr/share/info \
			--enable-shared \
			--enable-threads \
			$_openmp \
			$_cf
		make
	done
}

check() {
	local i; for i in $_precision; do
		cd "$srcdir"/$i
		make check
	done
}

package() {
	local i; for i in $_precision; do
		cd "$srcdir"/$i
		make DESTDIR="$pkgdir" install
	done
}

single() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libfftw3f*.so* \
		"$subpkgdir"/usr/lib/
}

double() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libfftw3*.so* \
		"$subpkgdir"/usr/lib/
}

long_double() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libfftw3l*.so* \
		"$subpkgdir"/usr/lib/
}

sha512sums="ab918b742a7c7dcb56390a0a0014f517a6dff9a2e4b4591060deeb2c652bf3c6868aa74559a422a276b853289b4b701bdcbd3d4d8c08943acf29167a7be81a38  fftw-3.3.8.tar.gz"
