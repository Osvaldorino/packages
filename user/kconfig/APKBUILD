# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kconfig
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for managing software configuration"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require running D-Bus.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfig-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R kconfigcore
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6c2bbf8aabef7344e5e085664d77c50b97541f2412e2c60f64f76b553034369b7a25cfbdd251f2b62e3bb91c2cd98749051cea3855ed09fb5baf80a098274242  kconfig-5.74.0.tar.xz"
