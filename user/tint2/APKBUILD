# Contributor: Mari Hahn <mari.hahn@wwu.de>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=tint2
pkgver=16.7
pkgrel=0
pkgdesc="Simple, unintrusive panel/taskbar"
url="https://gitlab.com/o9000/tint2"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only AND GPL-2.0+ AND MIT AND Custom:zlib-optional-acknowledgement"
depends=""
makedepends="cmake imlib2-dev glib-dev pango-dev cairo-dev
	libxcomposite-dev libxdamage-dev libxinerama-dev libxrandr-dev
	gtk+2.0-dev librsvg-dev startup-notification-dev linux-headers"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.bz2::https://gitlab.com/o9000/$pkgname/repository/v$pkgver/archive.tar.bz2"

prepare() {
	mv "$srcdir/$pkgname-v$pkgver"-* "$builddir"
	default_prepare
}

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="bd5cc461199d38028b81d84f30e896e658274e166e344f4bc98bc6e43c4e25790bc80a7d4aac48d4e9e8284137fd31e4019f0c1b956149222e67a66b42379f1d  tint2-16.7.tar.bz2"
