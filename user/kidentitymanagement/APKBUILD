# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kidentitymanagement
pkgver=20.08.1
pkgrel=0
pkgdesc="Identity management library for KDE"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kconfig-dev kcoreaddons-dev kiconthemes-dev kio-dev kjobwidgets-dev
	kpimtextedit-dev kservice-dev ktextwidgets-dev kxmlgui-dev solid-dev
	sonnet-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f31e44e5f94b0fc3209a4f5d5e9f638f6dafa36fdbb71a5e50ae450f34701b8eb431f5618f6fc74837b9b9f91d8906a7d04f544c69ba5cf6247c415a3f30c593  kidentitymanagement-20.08.1.tar.xz
23c745d4479e8eed9f855ff8e5333547457c2dc33fc09ad81cc3ba27acf6a3db2d7ed6e4ec455d6590e817eb8274d50d728768ee1366b0855b488bc6de524b34  lts.patch"
