# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=marble
pkgver=20.08.1
pkgrel=0
pkgdesc="Free, open-source map and virtual globe"
url="https://marble.kde.org/"
arch="all"
options="!check"  # Test suite requires package to be already installed.
license="LGPL-2.1-only AND GPL-2.0-only"
depends="shared-mime-info"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev zlib-dev
	qt5-qtserialport-dev krunner-dev kcoreaddons-dev kwallet-dev knewstuff-dev
	kio-dev kparts-dev kcrash-dev ki18n-dev phonon-dev plasma-framework-dev
	qt5-qtpositioning-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://download.kde.org/stable/release-service/$pkgver/src/marble-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

libs() {
	pkgdesc="$pkgdesc (libraries)"
	local file=
	for file in "$pkgdir"/usr/lib/lib*.so.[0-9]*; do
		mkdir -p "$subpkgdir"/usr/lib
		mv "$file" "$subpkgdir"/usr/lib/
	done
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4306ba2ca620d7081b289b309abca9d741ee3376583ee662b34d09a1b03b42e00f034c34be371fed03a1196df5dd21fecf7e6c981053acfc9511a2bd85417485  marble-20.08.1.tar.xz"
