# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lokalize
pkgver=20.08.1
pkgrel=0
pkgdesc="Computer-aided translation system"
url="https://kde.org/applications/development/org.kde.lokalize"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev qt5-qtscript-dev cmake extra-cmake-modules kauth-dev
	kcodecs-dev kcompletion-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev kiconthemes-dev kio-dev kitemviews-dev kjobwidgets-dev
	knotifications-dev kparts-dev kross-dev kservice-dev ktextwidgets-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev solid-dev sonnet-dev
	hunspell-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/lokalize-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9ac2990ee4147114e42ae0098e65ea608a8052d836b5d8a4a4b721d1fa631f6e40a21431ef8c00c03adb7fcc668ea76704f3be207c74347480c33f59ad4f6a22  lokalize-20.08.1.tar.xz
75b6e97aec4941b7d9ae2d84df58a5f9554169d91dffbf765ee780f8d408bb8aad1f581d8213194df843159677dde9170663740ca78ac7ac3655165dae1fea26  lts.patch"
