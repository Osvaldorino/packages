# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: 
pkgname=v4l-utils
pkgver=1.18.0
pkgrel=0
pkgdesc="Userspace tools and conversion library for Video 4 Linux"
url="https://www.linuxtv.org/wiki/index.php/V4l-utils"
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev libjpeg-turbo-dev argp-standalone linux-headers
	eudev-dev alsa-lib-dev"
subpackages="$pkgname-dev $pkgname-doc qv4l2 $pkgname-libs ir_keytable"
source="https://www.linuxtv.org/downloads/v4l-utils/$pkgname-$pkgver.tar.bz2
	qv4l2.svg
	qv4l2.desktop
	getsubopt.patch
	time64.patch
	types.patch
	"

build() {
	export CFLAGS="$CFLAGS -D__off_t=off_t"
	LIBS="-largp -lintl" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-libdvbv5 \
		--disable-static
        make
}

check() {
	make check
}

package() {
        make -j1 DESTDIR="$pkgdir" install
	install -Dm644 "$srcdir"/qv4l2.desktop \
		"$pkgdir"/usr/share/applications/qv4l2.desktop
	install -Dm644 "$srcdir"/qv4l2.svg \
		"$pkgdir"/usr/share/icons/hicolor/scalable/apps/qv4l2.svg
}

qv4l2() {
	pkgdesc="Qt V4L2 test control and streaming test application"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/qv4l2 "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share "$subpkgdir"/usr/
}

libs() {
	pkgdesc="Collection of Video4Linux support libraries"
	mkdir -p "$subpkgdir"/usr/lib
        mv "$pkgdir"/usr/lib/* \
                "$subpkgdir"/usr/lib
}

ir_keytable() {
	pkgdesc="Alter keymaps of Remote Controller devices"
	mkdir -p "$subpkgdir"/lib/udev/rc_keymaps \
		"$subpkgdir"/lib/udev/rules.d \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/etc
	mv "$pkgdir"/lib/udev/rc_keymaps/* \
                "$subpkgdir"/lib/udev/rc_keymaps
	mv "$pkgdir"/lib/udev/rules.d/* \
		"$subpkgdir"/lib/udev/rules.d
	mv "$pkgdir"/usr/bin/ir-keytable \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/etc/rc_maps.cfg \
		"$subpkgdir"/etc
}

sha512sums="749c804671f039d907bf782cba5d1c23fff48066f3b6d4a0249fb9b65c493ee3945b544ee2a306dda79973da04f0301278ee88775f798aba590e5d0f15226d49  v4l-utils-1.18.0.tar.bz2
bc18280046c15b19984103f7c2bb44a0aea79715803c64f0c64bc932499c09022c956914c3b15ae59499adc09f6fbff5378be45707fe851250f495a26b63d682  qv4l2.svg
6f74aa524b3de420eeb8de788ff3f717020732a3f1f6530caee50e63aae7eddbe5f551ffc50065c9f5d6078c13bace089948ecdcacf01f8b82c1a44960e06315  qv4l2.desktop
a09554deebd7597355c688e52180e0f4030842ccb26a144d1ac8a426836374237725d7b6b555027ca72e10c11e37bd596c7d69d87ee2f6b6d951daf4e50137f8  getsubopt.patch
16154d57ea0b2e231217894495ef4ecf55d45e97c936034d4bbb0dab88f2d3e8779073302fbba759bd60e124a2cefe2b83675952cd50c413d957c81c9b0b8b53  time64.patch
ee1228cc06e399923953e5121a683347a7fe881c80c55faae0adf6ed836251da1b2c4d1561454daa6f42220c1171f827ca3e2b72cdac31b379495f23ff6c56bb  types.patch"
