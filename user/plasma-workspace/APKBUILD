# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=plasma-workspace
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE Plasma 5 workspace"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test requires X11 accelration.
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0+ AND MIT AND LGPL-2.1-only AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
# startkde shell script calls
depends="kinit qdbus qtpaths xmessage xprop xset xsetroot"
# font installation stuff
depends="$depends mkfontdir"
# QML deps
depends="$depends kquickcharts qt5-qtgraphicaleffects qt5-qtquickcontrols solid"
# other runtime deps / plugins
depends="$depends libdbusmenu-qt kcmutils kde-cli-tools kded kdesu kio-extras
	ksysguard kwin milou plasma-integration pulseaudio-utils iso-codes xrdb"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kdelibs4support-dev
	kitemmodels-dev kservice-dev kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtscript-dev
	iso-codes-dev libdbusmenu-qt-dev libqalculate-dev libxtst-dev
	xcb-util-image-dev libkworkspace-dev

	baloo-dev kactivities-dev kcmutils-dev kcrash-dev kdbusaddons-dev
	kdeclarative-dev kdesu-dev kdoctools-dev kglobalaccel-dev kholidays-dev
	kidletime-dev kjsembed-dev knewstuff-dev knotifyconfig-dev kpackage-dev
	krunner-dev kscreenlocker-dev ktexteditor-dev ktextwidgets-dev
	kwallet-dev kwayland-dev kwin-dev kxmlrpcclient-dev libksysguard-dev
	plasma-framework-dev prison-dev kactivities-stats-dev kpeople-dev
	kirigami2-dev kuserfeedback-dev libkscreen-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	libkworkspace.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="870cf89649d9498831f4ef9b21d3c07504b7fc7b09b95dd7e0a1d356b41fbfceed1c4f27aa258bcf1e23cfe915d31701c155325fcd4944f9cc957a287ebc1ee2  plasma-workspace-5.18.5.tar.xz
f58b88928fd68518bc0524db35388cb0f0dbc2a55d85fc47e92ce7fcbaf9b155482736e282bd84104ceecc625406845840128c8d0fcd2a4d5a854673964cd94f  libkworkspace.patch"
