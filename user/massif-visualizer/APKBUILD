# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=massif-visualizer
pkgver=0.7.0
pkgrel=0
pkgdesc="Visual tool for inspecting Massif profiler data"
url="https://kde.org/applications/development/org.kde.massif-visualizer"
arch="all"
license="GPL-2.0"
depends="kgraphviewer shared-mime-info"
makedepends="qt5-qtbase-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev cmake
	extra-cmake-modules karchive-dev kauth-dev kcodecs-dev kcompletion-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdiagram-dev ki18n-dev
	kio-dev kitemviews-dev kjobwidgets-dev kparts-dev kservice-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev solid-dev sonnet-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/massif-visualizer/$pkgver/src/massif-visualizer-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="058412e34c0899a34fb4080a602cb0c70a9ac707f27b72a79fb412ea35a6e8c66ce06df15b3153fdc1a70a27232f98b508c7441b657eb73466962b33f9b2f561  massif-visualizer-0.7.0.tar.xz"
