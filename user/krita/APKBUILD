# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krita
pkgver=4.3.0
pkgrel=0
pkgdesc="Digital painting program by KDE"
url="https://krita.org/"
arch="all"
options="!check"  # Tests require X11.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kwidgetsaddons-dev kcompletion-dev kcoreaddons-dev kguiaddons-dev
	ki18n-dev kitemmodels-dev kitemviews-dev kwindowsystem-dev zlib-dev
	qt5-qtsvg-dev qt5-qtmultimedia-dev kcrash-dev libice-dev libx11-dev
	libxcb-dev fftw-dev libpng-dev boost-dev tiff-dev libjpeg-turbo-dev
	kio-dev eigen-dev exiv2-dev lcms2-dev poppler-qt5-dev gsl-dev libxi-dev
	libraw-dev quazip-dev giflib-dev poppler-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/krita/4.3.0/krita-$pkgver.tar.gz
	fix-nullptr-deref.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		-DBUILD_TESTING=False \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="2f60d407a9e143aac289b16ba442619660d3ec56492c5a85095004d724378a682661d4594a0c3aec9b10cc225c45058d7890be3d05ce4460fa807eca59020509  krita-4.3.0.tar.gz
fd43f21da937c29f550e24d459e83ad9714befe82fe0cae493b28cad6526b11f80a5e3d350be1fa910a62c07284774a6f5fac4bbf4801be378987fbfaef61254  fix-nullptr-deref.patch"
