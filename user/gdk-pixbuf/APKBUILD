# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gdk-pixbuf
pkgver=2.40.0
pkgrel=0
pkgdesc="GTK+ image loading library"
url="https://www.gtk.org/"
arch="all"
options="!check"  # bug753605-atsize.jpg is missing from tarball.
license="LGPL-2.0+"
depends="shared-mime-info"
makedepends="glib-dev gobject-introspection-dev libjpeg-turbo-dev libpng-dev
	meson ninja python3 tiff-dev xmlto"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/gdk-pixbuf-2.0/*/loaders"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/gdk-pixbuf/${pkgver%.*}/gdk-pixbuf-$pkgver.tar.xz"

# secfixes:
#   2.36.6-r1:
#     - CVE-2017-6311
#     - CVE-2017-6312
#     - CVE-2017-6314

build() {
	meson \
		-Dprefix=/usr \
		-Dinstalled_tests=false \
		build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

sha512sums="6512befd379494dbfd89a16fc4c92641842eb7b82fc820ec83a5b057526209947db646570db1124c073b0ef69c117bdf0f8d3fea807e302a4950db39187c35aa  gdk-pixbuf-2.40.0.tar.xz"
