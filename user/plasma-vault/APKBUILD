# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-vault
pkgver=5.18.5
pkgrel=0
pkgdesc="Secure storage plugin for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="encfs"
makedepends="cmake extra-cmake-modules kactivities-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev ki18n-dev kio-dev
	kwidgetsaddons-dev libksysguard-dev plasma-framework-dev
	qt5-qtbase-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-vault-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="66fcd87dee7e696aa0f5fa41adcb41134bb0da1ee9920f51057ee8729f8d88052b28f4e422795fc229767b7861226d1386086748afb356f4cb7281c1da4af7e8  plasma-vault-5.18.5.tar.xz"
