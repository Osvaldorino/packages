# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=utmps
pkgver=0.1.0.0
pkgrel=0
pkgdesc="A secure utmp/wtmp implementation"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite
license="ISC"
depends="execline s6"
_skalibs_version=2.10.0.0
makedepends="skalibs-dev>=$_skalibs_version"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
install="$pkgname.post-upgrade"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz
	utmpd.run
	wtmpd.run"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--enable-allstatic \
		--enable-static-libc \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib \
		--enable-libc-includes
	make
}

package() {
	make DESTDIR="$pkgdir" install
	runimage="$pkgdir/etc/s6-linux-init/current/run-image"
        mkdir -p -m 0755 "$runimage/utmps" "$runimage/service/utmpd" "$runimage/service/wtmpd"
	chown utmp:utmp "$runimage/utmps"
        cp -f "$srcdir/utmpd.run" "$runimage/service/utmpd/run"
	echo 3 > "$runimage/service/utmpd/notification-fd"
        cp -f "$srcdir/wtmpd.run" "$runimage/service/wtmpd/run"
	echo 3 > "$runimage/service/wtmpd/notification-fd"
        chmod 0755 "$runimage/service/utmpd/run" "$runimage/service/wtmpd/run"
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
        install_if="dev $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr"
        mv "$pkgdir/usr/lib" "$pkgdir/usr/include" "$subpkgdir/usr/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=""
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="f8d246897690829fccc6e4a4e1bfbbbacd10d337863dacb6966f62cffc166d61c49db1ac83d51aa023e571a5d62f81cf030fa439eb8e3fe742e427764f18adcf  utmps-0.1.0.0.tar.gz
0ec30284c64c6ea9f25142c5f4a643bd48b137fe85781b650104f5137ffa4dfc35ca7be3e41e3acd3403ebe1d8c5378073afa4e2f3607d3d794fcd9f98ed51c4  utmpd.run
cba4f2ec3b8f5becf3ae57eecf584745d783046ee6cf5d116322421ad5ffd074d2955da22d31d2b5b1d05f906378aae92f221d2ac95ac21b54a361fbdc0566e7  wtmpd.run"
