# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bison
pkgver=3.7.3
pkgrel=0
pkgdesc="The GNU general-purpose parser generator"
arch="all"
# iconv issue:
# https://lists.gnu.org/archive/html/bug-bison/2020-07/msg00001.html
# https://www.openwall.com/lists/musl/2020/07/29/2
options="!check"  # see above
license="GPL-3.0+"
url="https://www.gnu.org/software/bison/bison.html"
depends="m4"
checkdepends="bison flex musl-locales"
makedepends="perl"
provider_priority=1
subpackages="$pkgname-doc $pkgname-lang"
source="https://ftp.gnu.org/gnu/bison/${pkgname}-${pkgver}.tar.xz"

# secfixes:
#   3.7-r0:
#     - CVE-2020-24240
#   3.5.4-r0:
#     - CVE-2020-14150

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--datadir=/usr/share \
		--infodir=/usr/share/info \
		--mandir=/usr/share/man
	make
}

check() {
	# Work around for Bison 3.4.x test failure
	for _calc in calc lexcalc mfcalc reccalc rpcalc; do
		make -j1 examples/c/$_calc/$_calc
	done
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="34fe630749dc015f15a830ad13742c4b0b9163143a11e987c16954bee0451e819ec2fa7b43bd34c867e5ce72c758eb9070348d1d581c9435b9531a99caa46eed  bison-3.7.3.tar.xz"
