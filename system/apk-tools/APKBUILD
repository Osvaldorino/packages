# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=apk-tools
pkgver=2.12.0
pkgrel=0
pkgdesc="Alpine Package Keeper - package manager"
url="https://git.adelielinux.org/adelie/apk-tools"
arch="all"
license="GPL-2.0-only"
depends="ca-certificates"
makedepends_build=""
makedepends_host="zlib-dev openssl openssl-dev linux-headers"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-dev $pkgname-static $pkgname-doc"
source="https://dev.sick.bike/dist/$pkgname-v$pkgver+adelie.tar.gz
	https://dev.sick.bike/dist/$pkgname-help-v$pkgver.tar.gz
	apk.zsh-completion
	s6-linux-init.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	sed -i -e 's:-Werror::' Make.rules
	cat >config.mk <<-EOF
		FULL_VERSION=$pkgver-r$pkgrel
		LUA=no
		export LUA
	EOF
}

build() {
	make
	make static
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -d "$pkgdir"/var/lib/apk \
		"$pkgdir"/var/cache/misc \
		"$pkgdir"/etc/apk/keys \
		"$pkgdir"/etc/apk/protected_paths.d
	# the shipped README is not useful
	rm -r "$pkgdir"/usr/share/doc

	install -Dm644 "$srcdir"/apk.zsh-completion \
		"$pkgdir"/usr/share/zsh/site-functions/_apk
}

static() {
	pkgdesc="Alpine Package Keeper - static binary"
	install -Dm755 "$builddir"/src/apk.static \
		"$subpkgdir"/sbin/apk.static

	# Sign the static binary so it can be vefified from distros
	# that do not have apk-tools
	local abuild_conf="${ABUILD_CONF:-"/etc/abuild.conf"}"
	local abuild_home="${ABUILD_USERDIR:-"$HOME/.abuild"}"
	local abuild_userconf="${ABUILD_USERCONF:-"$abuild_home/abuild.conf"}"
	[ -f "$abuild_userconf" ] && . "$abuild_userconf"
	local privkey="$PACKAGER_PRIVKEY"
	local pubkey="${PACKAGER_PUBKEY:-"${privkey}.pub"}"
	local keyname="${pubkey##*/}"
	${CROSS_COMPILE}strip "$subpkgdir"/sbin/apk.static
	openssl dgst -sha1 -sign "$privkey" \
		-out "$subpkgdir/sbin/apk.static.SIGN.RSA.$keyname" \
		"$subpkgdir"/sbin/apk.static
}

sha512sums="739cbf487a9184aba56c87448d18acebf357b6eddb4852033cc0cd331fc910d425a5d01f7b0bf109f5cd62f78f10cca96ebcbdb4fa00d197edb90726998e1523  apk-tools-v2.12.0+adelie.tar.gz
b8e6e6e9efe2fc3a900e5fea54ed2f047bbf8917c8c3e470e001865c2169167ce6a273869ba577aceb1a1580b7dd684de115a4d92250786958fa13b9e8abf28b  apk-tools-help-v2.12.0.tar.gz
cedda9bf11e0a516c9bf0fd1a239ffa345cdd236419cbd8b10273410610ae7d0f0f61fd36e1e9ccc3bbf32f895508cdca4fb57a0e04f78dd88469b33bf64a32a  apk.zsh-completion
f92e2e0c062b71e1e5cf95f0d4997166ccc7f7a5e45af8d1650f5951a1d552d89217c8c60d24f31fa626e8e0675c5e882e6b36ef1af8f7624e54627b22801381  s6-linux-init.patch"
